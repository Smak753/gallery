package com.example.gallary

import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.File
import java.io.FileOutputStream
import java.net.URL




class MainActivity : AppCompatActivity() {
    var CameraRequestCode = 0
    var adapterView = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        var layoutManager =GridLayoutManager(applicationContext, 3)
        photoLayout.layoutManager = layoutManager
        updateArray(PhotoList(adapterView))
        fabClicklistener()
        photoLayout.addOnScrollListener(
                OnScrollListener(layoutManager))


    }


    fun updateArray(data: List<File>) {
        var photo = data
        var adapter = PhotoAdapter(photo)
        adapter.notifyDataSetChanged()
        photoLayout.adapter = adapter
    }


    override fun onResume() {
        super.onResume()
        fabClicklistener()
        updateArray(PhotoList(adapterView))
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var id = item.itemId
        when (id) {
            R.id.action_all -> {
                adapterView = 1
                updateArray(PhotoList(adapterView))
                return true
            }
            R.id.action_url -> {
                btnCancel.visibility = View.VISIBLE
                inputUrl.visibility = View.VISIBLE
                var clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                inputUrl.setText(clipboard.text)
                fab.setImageResource(R.drawable.ic_menu_btn_add)
                fab.setOnClickListener({
                    var url = inputUrl.text.toString()
                    if (url == "" || !url.endsWith(".jpg")) {
                        Toast.makeText(applicationContext,
                                getString(R.string.input_url_exception),
                                Toast.LENGTH_SHORT).show()
                    } else {
                        fabClicklistener()
                        var task = MyTask()
                        task.execute(url)
                        updateArray(PhotoList(adapterView))

                    }
                })
                btnCancel.setOnClickListener({
                    fabClicklistener()
                })
                return true
            }

        }
        return false
    }

    fun fabClicklistener() {
        inputUrl.visibility = View.INVISIBLE
        btnCancel.visibility = View.INVISIBLE
        fab.setImageResource(R.drawable.ic_menu_camera)
        fab.setOnClickListener({
            var values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "New Picture")
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
            var imageUri = contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(cameraIntent, CameraRequestCode)
        })
    }

    inner class MyTask : AsyncTask<String, Void, Bitmap>() {
        override fun doInBackground(vararg p0: String?): Bitmap {
            var file = File(Environment.getExternalStoragePublicDirectory(Environment.
                    DIRECTORY_PICTURES), System.currentTimeMillis().toString() + ".jpg")
            file.createNewFile()

            var fos = FileOutputStream(file)
            var url = URL(p0[0])
                fos.write(url.readBytes())
                fos.close()
                return BitmapFactory.decodeFile(file.absolutePath)
        }
    }
}


