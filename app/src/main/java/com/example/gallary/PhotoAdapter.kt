package com.example.gallary


import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import java.io.File
import kotlinx.android.synthetic.main.item.view.*




class PhotoAdapter(var data:List<File>): RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {
    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        try
        {
            holder.photo.setImageBitmap(BitmapFactory.decodeFile(data[position].absolutePath))
        }
        catch (e: OutOfMemoryError)
        {
            var options = BitmapFactory.Options()
            options.inSampleSize = 8
            holder.photo.setImageBitmap(BitmapFactory.decodeFile(data[position].absolutePath, options))
        }

        holder.photo.setOnClickListener ({
        var intent = Intent(holder.photo.context,PhotoShow::class.java)
            intent.putExtra("name",data.get(position).name)
            intent.putExtra("img",data[position].absolutePath)
            holder.photo.context.startActivity(intent)
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder=
            PhotoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    override fun getItemCount(): Int=data.size
    class PhotoViewHolder(view: View):RecyclerView.ViewHolder(view){
        var photo: ImageView = view.photoView

    }


}