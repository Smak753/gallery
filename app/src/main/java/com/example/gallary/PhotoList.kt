package com.example.gallary

import android.os.Environment
import java.io.File
import java.util.ArrayList

fun PhotoList(all:Int=0):List<File> {
    var dcim = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DCIM)
    var pictures = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES)
        var picFiles = getListFiles(dcim)
        picFiles.addAll(getListFiles(pictures))

    if(all!=0){
        var all = Environment.getExternalStorageDirectory()
        picFiles = getListFiles(all)
    }

    var sortedPic = picFiles.sortedWith(kotlin.Comparator(
            fun(lhs: File, rhs: File): Int {
                return lhs.lastModified().compareTo(rhs.lastModified())
            }
    )
    )
    return sortedPic
}
fun getListFiles(parentDir: File): ArrayList<File> {
    val listFile = parentDir.listFiles()

    var files = ArrayList<File>()
    for (f: File in listFile) {
        if (f.isDirectory) {
            files.addAll(getListFiles(f))
        } else {
            if (f.name.endsWith(".jpg") || f.name.endsWith(".jpeg")|| f.name.endsWith(".png"))
                files.add(f)
        }
    }
    return files
}