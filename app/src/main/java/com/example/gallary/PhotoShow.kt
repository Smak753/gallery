package com.example.gallary

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.AnimationUtils

import kotlinx.android.synthetic.main.activity_photo_show.*
import kotlinx.android.synthetic.main.content_photo_show.*
import java.io.File
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.view.KeyEvent


class PhotoShow : AppCompatActivity() {
    var deleteBtnAcept= 0
   open override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_show)
        var  animAlpha = AnimationUtils.loadAnimation(this, R.anim.alpha)
        textView.setText(intent.getStringExtra("name"))
        var takePhoto =intent.extras["img"].toString()
        var filePhoto =File(takePhoto)
        try {
            photoShow.setImageBitmap(BitmapFactory.decodeFile(takePhoto))
        }
        catch (e:OutOfMemoryError){
            var options = BitmapFactory.Options()
            options.inSampleSize = 5
            photoShow.setImageBitmap(BitmapFactory.decodeFile(takePhoto, options))
        }
        textView.setEnabled(false)
        btnAccept.visibility = View.INVISIBLE

        btnDelete.setOnClickListener({
            showDialog(deleteBtnAcept)
        })
        btnShare.setOnClickListener({
            btnShare.startAnimation(animAlpha)
            var shareIntent=Intent(Intent.ACTION_SEND)
            shareIntent.type="image/*"
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(filePhoto))
            startActivity(Intent.createChooser(shareIntent, "Share Image"))
        })
        btnChange.setOnClickListener({
            btnChange.startAnimation(animAlpha)
            textView.setEnabled(true)
            btnAccept.visibility = View.VISIBLE

        })
        btnAccept.setOnClickListener({
            var newName =File(filePhoto.parent,
                    textView.text.toString()+"."+filePhoto.extension)
            filePhoto.renameTo(newName)
            textView.setEnabled(false)
            btnAccept.visibility = View.INVISIBLE
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == (KeyEvent.KEYCODE_BACK)) {
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    fun deleteBtnOption(){
    var  animAlpha = AnimationUtils.loadAnimation(this, R.anim.alpha)
    var takePhoto =intent.extras["img"].toString()
    var filePhoto =File(takePhoto)
    btnDelete.startAnimation(animAlpha)
    filePhoto.delete()
    finish()
}

    override fun onCreateDialog(id: Int, args: Bundle?): Dialog {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.accept_delete)+"\n"+textView.text.toString())
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.accept),
                                DialogInterface.OnClickListener { dialog, id ->
                                    dialog.cancel()
                                    deleteBtnOption()
                                })
                        .setNegativeButton(getString(R.string.btnCancel),
                                DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

                return builder.create()

    }

}
