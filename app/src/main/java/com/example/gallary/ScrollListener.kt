package com.example.gallary


import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView

class OnScrollListener(var layoutManager: GridLayoutManager) : RecyclerView.OnScrollListener() {
    var previousTotal = 0
    var loading = true
    val visibleThreshold = 10
    var firstVisibleItem = 0
    var lastVisibleItem=0
    var visibleItemCount = 0
    var totalItemCount = 0
    var currentPage = 0
    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        visibleItemCount = recyclerView!!.childCount
        totalItemCount = recyclerView!!.layoutManager.itemCount
        firstVisibleItem = layoutManager!!.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            currentPage++
            loading = true
        }
    }
}